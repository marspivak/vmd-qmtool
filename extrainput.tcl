
$m entryconfigure 0 -command {
   .qm_setup.edit.guessbutton configure -state normal
   .qm_setup.edit.coordbutton configure -state disabled
   .qm_setup.edit.type.hindrot configure -state disabled
}
$m entryconfigure 1 -command {
   .qm_setup.edit.guessbutton configure -state normal
   .qm_setup.edit.coordbutton configure -state normal
   .qm_setup.edit.type.hindrot configure -state disabled
}
$m entryconfigure 2 -command {
   .qm_setup.edit.guessbutton configure -state normal
   .qm_setup.edit.coordbutton configure -state normal
   .qm_setup.edit.type.hindrot configure -state normal
}
$m entryconfigure 3 -command {
   .qm_setup.edit.guessbutton configure -state disabled
   .qm_setup.edit.coordbutton configure -state normal
   .qm_setup.edit.type.hindrot configure -state disabled
}

if {$::QMtool::calcnbo} { 
#.qm_setup.edit.nbolewis.check configure -state normal
} else {
#.qm_setup.edit.nbolewis.check configure -state disabled
}
}
#button $v.okcancel.gamess  -text "Write GAMESS input file" \
   -command {}
#tk_messageBox -icon error -type ok -title Message -parent .qm_setup \
   -message "Sorry, this feature will be available in a future version!"
#{ }

#label $v.edit.checkfilelabel -text "Checkpoint file:"
#entry $v.edit.checkfileentry -textvariable ::QMtool::checkfile -width 60
#grid $v.edit.checkfilelabel -column 0 -row 1 -sticky w
#grid $v.edit.checkfileentry -column 1 -row 1 -sticky w

#    foreach amethod $availmethods {
#       set pos [lsearch $availmethods $amethod]
#       if {[string match {CBS-*} $amethod]} {
# puts "$pos $amethod"
# 	 $m entryconfigure $pos -vcmd {
# puts DISABLED
# 	    set ::QMtool::simtype "Single point"
# 	    .qm_setup.edit.type.simtypebutton configure -state disabled
# 	 }
#        } else {
# 	 $m entryconfigure $pos -command {
# 	    .qm_setup.edit.type.simtypebutton configure -state normal
# 	 }
#       }
#    }

#checkbutton $v.edit.dgsolv  -text "Calculate dG of solvation"  -variable ::QMtool::calcdGsolv
#grid $v.edit.dgsolv   -column 1 -row 13 -sticky w

#label $v.edit.chargeslabel -text "Charges:"
#checkbutton $v.edit.chargesesp  -text "ESP charges"  -variable ::QMtool::calcesp
#checkbutton $v.edit.chargesnpa  -text "NPA charges"  -variable ::QMtool::calcnpa
#grid $v.edit.chargeslabel -column 0 -row 14 -sticky w
#grid $v.edit.chargesesp   -column 1 -row 14 -sticky w
#grid $v.edit.chargesnpa   -column 1 -row 15 -sticky w

#    checkbutton $v.edit.type.simtypebutton.opt  -text "Geometry optimization" -variable ::QMtool::optimize \
#       -command { 
# 	 if {$::QMtool::optimize} {
# 	    .qm_setup.edit.guessbutton configure -state normal
# 	    .qm_setup.edit.coordbutton configure -state normal
# 	 } else {

# 	 }
#       }
#   checkbutton $v.edit.type.simtypebutton.freq -text "Frequency calculation" -variable ::QMtool::frequency
#   pack $v.edit.type.simtypebutton.opt $v.edit.type.simtypebutton.freq -anchor w

#checkbutton $v.edit.type.hindrot -text "Hindered Rotor Analysis" -variable ::QMtool::hinderedrotor
#pack $v.edit.type.hindrot -padx 3m -side left -anchor w


#label $v.edit.geomlabel -text "Geometry from:"
#set m [tk_optionMenu $v.edit.geombutton ::QMtool::geometry "Z-matrix" "Checkpoint file"]
#$m entryconfigure 0 -command {}
#  .qm_setup.edit.chargeentry configure -state normal
#    .qm_setup.edit.multipentry configure -state normal
#    if {$::QMtool::guess=="Guess (Harris)"} {}
# .qm_setup.edit.fcheckfileentry configure -state disabled
#    {}
   if {$::QMtool::guess=="Read geometry and wavefunction from checkfile"} {
set ::QMtool::guess "Take guess from checkpoint file"
   }
}
#$m entryconfigure 1 -command {}
   #.qm_setup.edit.fcheckfileentry configure -state normal
{}
#grid $v.edit.geomlabel -column 0 -row 8 -sticky w
#grid $v.edit.geombutton -column 1 -row 8 -sticky w

#label $v.edit.guesslabel -text "Initial wavefunction:"
#set m [tk_optionMenu $v.edit.guessbutton ::QMtool::guess "Guess (Harris)" \
    "Take guess from checkpoint file" "Read geometry and wavefunction from checkfile"]
#$m entryconfigure 0 -command {}
   #.qm_setup.edit.chargeentry configure -state normal
   #.qm_setup.edit.multipentry configure -state normal
   #if {$::QMtool::geometry=="Z-matrix"} {}
#.qm_setup.edit.fcheckfileentry configure -state disabled
   #{}
#{}
#$m entryconfigure 1 -command {}
   #.qm_setup.edit.chargeentry configure -state normal
   #.qm_setup.edit.multipentry configure -state normal
   #.qm_setup.edit.fcheckfileentry configure -state normal
#{}
#$m entryconfigure 2 -command {}
   #set ::QMtool::geometry "Checkpoint file"
   #.qm_setup.edit.chargeentry configure -state disabled
   #.qm_setup.edit.multipentry configure -state disabled
   #.qm_setup.edit.fcheckfileentry configure -state normal
#{}
#grid $v.edit.guesslabel -column 0 -row 9 -sticky w
#grid $v.edit.guessbutton -column 1 -row 9 -sticky w

#label $v.edit.coordlabel -text "Coordinates for Opt/Freq:"
#tk_optionMenu $v.edit.coordbutton ::QMtool::coordtype "Internal (auto)" \
   "Internal (explicit)" "ModRedundant" 
#grid $v.edit.coordlabel  -column 0 -row 10 -sticky w
#grid $v.edit.coordbutton -column 1 -row 10 -sticky w

#frame $v.edit.nbolewis
#checkbutton $v.edit.nbolewis.check -text "Specify Lewis structure in 'CHOOSE' statement  " \
   -variable ::QMtool::calcnboread 
#button $v.edit.nbolewis.edit -text "Edit Lewis structure in Molefacture" -command [namespace code {}]
 #  molefacture_start
#[{}]
#pack  $v.edit.nbolewis.check $v.edit.nbolewis.edit -side left

taken out from gaussian.tcl

if {$calcnpa} {
   lappend popkeys "NPA"
}

Callback for click inside plot window, at coords x y
proc ::QMtool::plot_clicked { x y } {
 variable ::QMtool::plothandle
 
 set ns [namespace qualifiers $plothandle]
 set xplotmin [set ${ns}::xplotmin]
 set xplotmax [set ${ns}::xplotmax]
 set yplotmin [set ${ns}::yplotmin]
 set yplotmax [set ${ns}::yplotmax]
 set scalex [set ${ns}::scalex]
 set xmin [set ${ns}::xmin]

 # note: ymax < ymin because of pixel coordinate convention
 if { [expr {($x < $xplotmin) || ($x > $xplotmax) || ($y > $yplotmin) || ($y < $yplotmax)}] } {
   return
 }

 animate goto [expr { ($x - $xplotmin) / $scalex + $xmin}]
 
}
